# Clear Air Turbulence (CAT)

![CAT0020](media/CAT0020Frontview.jpg)

The [CAT](https://en.wikipedia.org/wiki/List_of_spacecraft_in_the_Culture_series) is meant (largely) to serve as an easily-replicated, well known machine that can be put to use on a range of common fab processes: pcb milling (or light milling: i.e. molds), 3d printing (swappable extruder heads would be cool), pen plotting (etc), and knife-cutting...

A few design goals are held in mind for this machine that could make or break the possibility of this machine:
This machine is meant to be made with plastic using mainly a laser cutter and set of 3D printers. The first version of this was made using acrylic. Moving forward, Delrin will be used to prevent cracking, however, both are useable depending on your specific machine needs.

Total lasercut runtime: ~ 5 hrs
Total 3D print runtime: ~50 hrs

The machine CAD is also parametric, meaning you can tune the width, length, depth, structural strength, height, etc.


## Material Selection
A few different plastics can be used. We can talk here about the Pros and Cons of each one to help you better select a material to use.

Since we are laser cutting, Acrylic and Delrin are the two main options. If you have access to something like a router, perhaps different plastics can be considered.

Delrin ultimately would be the best option if you can. The thing about Delrin is that it's a bit tedious to cut 1/4" on a not-so-hot laser. If you have the ability to do 1/4" Delrin, that's definitely the best option as Acrylic can crack quite a bit. Another thing that we must take into account is hardness. Acrylic is harder than Delrin, and for the bearing rails, there is worry that use will cause heavy wear on Delrin. This will be something to keep an eye on towards the future.

## CAT 0020
This is prototype version 2 of the CAT.

![firstrun](media/CAT0020firstrun.mp4)

### Joining plastic sheets
In order to join the plastic laser cut sheets, 3d printed joinery is made. There is a few types of joinery. The bed uses HexTrompo joints. Named that because they look like the toy commonly used in Mexico: [Trompo](https://en.wikipedia.org/wiki/Trompo). These little dudes help tension the two bed sheets together with the webbing pieces sandwiched in the middle. There is a male one which lies on the top and a female one with a heat insert on the bottom sheet. An M4x55 bolt runs between them and keeps it all nice and taught. This way, all the loads that go on to the bed first come through a 3d printed part softening the load on the acrylic hopefully preventing some of the cracking if you use acrylic. A whole bed's worth should take about 10 printer hours to make.

![beam_joinery](media/beam_joinery.jpg)

Here is a unit cell of what the inside of the bed looks like:

![ht_unitcell](media/ht_unitcell.jpg)

For the side walls, a different kind of joint is used. These don't have a name yet so we'll call em simple beam joints here. They are designed to click in and be easy to put on and tighten.

Here is a unit cell of what these beam joints look like:

![bj_unit](media/bj_unit.jpg)

### Making the parts
From the fusion model, you can create dxfs of all the required pieces along with STLS for all the 3d prints.

Pro tip: many of the parts are mirrored from the left rail to the right rail, so do not use the same model for those mirrored parts. I.E. the motor mounts are mirrored and not the same.

### Assembly

#### bed

First up, you want to assemble the bed part. The bed should only take about an hour to an hour and a half to assemble. Make sure to heat insert the female HexTrompos and the beam joints before you start so that you can do this all in one go.

The side walls should go on first along with the rail webbings. These are the L-shaped webbings alternating between long and short. Tighten up all of these using the simple beam joints and M3x20 socket head bolts. Next up put female HexTrompos on the bottom of the bed. There is a little circle on the edge of the hex trompo which corresponds to the 1/4" webbings. Make sure your orientation is right otherwise it migth not slide in as you would like. This is because the hexagon is not a regular hexagon. In order to preserve horizontal and vertical round distances, a different angle is taken. These should click in and hold in place which should make assembly easier.

![(htb_side](media/htb_side.jpg)


Next up, Put in all of your webbings in the appropriate slots on the bed. In the model, they are overconstrained to make the fit nice and tight. Depending on your laser cutter, you might need to overconstrain even more. Slide the top 1/8" bed sheet on top of the webbings. Here you'll have to finaggle the webbings to the holes. Go from one corner to the other and eventually all the webbings will fit. I found it helpful to use a small screwdriver to move the webbings needed from the top using the HexTrompo holes. Once everything is nice and flush (make sure that all the webbings are actually in the slots), start attaching the male hextrompos and the M4x55's to tighten it all up.

WARNING: Plastic deforms easily so **DO NOT OVERTIGHTEN**.

Now install the rails onto the bed using more of the simple beam joints and M3x20 Flat head bolts. Make sure to countersink the holes so that the bolt-head top is flush or underneath the top.

While you're at it here, make and install the verticall pull mounts and the end stops.

#### carriage

The parts for the carriage can be easily identified from the CAD model, cut and printed. For how to assemble these look at these links:

The carriage we use here uses an older beam model. Info about this model can be found here:https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/beams

For information on the bearing side of things along with how to assemble the bearing kits can be found here: https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/gantries/n17_linearPinion

#### Z axis

The Z axis is very similar to the carriage except smaller and inverse, so info can be found here as well: https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/gantries/n17_linearPinion

Just make sure you use the parts in the CAD model

#### Energy chains.
Make two of these for the Y and X axis': [link](https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/tapeChains). These will let you wire this puppy (cat?) up with no problem.

#### Putting it all together

The main components are the bed, the carriage (X axis), and the Z axis. I'll be referring to them like this from now on. It should be fairly obvious which one is which.

Before you start:
- I attached a ton of pictures in [here](https://gitlab.cba.mit.edu/jakeread/clearairturbulence/tree/master/media) so look at those as need be.

I would first install the z axis onto the carriage. It goes on the kinematic mount with two M4 bolts. Then you should zip tie the end of the wiring harness that goes to the z axis. It attaches to a mount that sticks out of the top of the carriage.

Next, put in the guides for the wiring harness on the side of the bed. These are the pink 3d printed parts that look like right triangles.

Slide the carriage into the bed rails from the front, and then zip tie the wiring harness to the pink wiring harness guides.

Lastly, install the violet motor mounts at the front of the rails centered on the holes. Stretch out the belts and run it through the pulley on the rail and grab the pulley pinions (GT2 20T B5) and install onto the motor mount along with the belt. The vertical position of the pulley matters somewhat, so look at a picture and try to mimic it. Tighten up the motor afterward until you think belts are nice and tight.

#### Electronics

Here is a picture of the Duet3D layout to help tell what is what: https://d17kynu4zpq5hy.cloudfront.net/igi/duet3d/vqBUAZPsxMC5tRgt.full

There is also a picture of the board connected with these motors onto the CAT 0020 attached.

Mount the Duet 3D with M4 bolts with the motor connector side toward the front onto the Duet 3D printed mount. Plz don't super tighten those. They're supposed to be loose because the mount I designed is super bad and you don't want to bend the board. Connect power (+ is labeled) and the motors. the X axis goes on  Drive 0, the Y axis motors go on Drive 1 and 2 (The right side motor on Drive 2), and the Z on Drive 3.

That should be it for that? Make sure Black/brown matches the way it is in the picture of the Duet3D layout.


#### Software
I'm gonna leave this part to Duet3D's guide. It's super detailed and easy to follow. You can find it here:
https://duet3d.dozuki.com/Wiki/Step_by_step_guide

Eventually, you'll get to the configurator to setup the Duet3D with this machine which is online here: https://configurator.reprapfirmware.org/

It's super intuitive and here are the values I changed for the CAT 0020:

General Tab:
Cartesian:
X range(0-325) Y range(0-330) Z range (0-50) <this Z range is too big with the bed, you can change it to 0-34 if you want hard stop including the bed which you probably want.

Motors Tab:
Axes:
All drives shoudl be x16 microstepping, 80 steps/mm, 15 mm/s speed change, max speed of 500 mm/s,  1000mm/s^2, and 800 mA of motor current. These are not tuned AT ALL. I turned them up to get better performance, but I didn't test too much, so feel free to reconfigure later if you want and send me back what you put so I can reconfigure here too.

The Z axis should say that the  Motor driver is on 3. (this doesn't matter though I think because it gets overriden later anyway...)

One thing that matters is the IP address on the Network page. What I did is I took the IP address that it connected to dynamically in YAT when I had it first connect to WiFi and just copied that into the  IP Address slot after you uncheck acquire dynamic IP address. I'm not too knowledgeable on networking though so I'm not sure how this will work out over there. This might be a place where you run into trouble. I did read that it was possible to still send gcode with just the USB connection through like YAT though, so since the hardware is already configured here, it should still work through that worst case scenario?

In the Finish Tab, it's important that you add the following two lines to the Custom settings (just copy paste it). It just sets up the duet to use 2 motors for the y axis, and reverses one so they don't fight each other. This also means that the silkscreen markings for the Axes on the duet are wrong with this setup though so beware there.

M584 X0 Y1:2 Z3 E4:5 ; set 2 and 3 to y axis, whilst putting z in motor 3 slot  
M569 P2 S0 ; reverse one of the y motors (Motor 2)



The rest I leave at default or if they're obvious like the endstops setting to None or setting Heat Bed to none. If you can figure out the stall detection though, that would be awesome. It didn't work when I tried it out a few times with different parameters, but with more time, it might be usable.
After you finish configurator you just upload the zip using the web tool.



#### G-Code
This website details all the codes it accepts and it's super useful: https://duet3d.dozuki.com/Wiki/Gcode

The main ones I used to test and home:

M18 - disables motors.
G92 X0 Y0 Z0 set this x,y,z position to be 0 (if you only do G92 X0, it will only set X to 0)
G90 - absolute positioning
G91 - Relative positioning
G0/G1 Xnnn Ynnn Znnn Fnnnn  Jog to x,y,z there with speed Fnnnn where nnnn here is the mm/min. I found F30000 to be around the maximum. After that it can skip and stuff. Here too, you can just say X50 or Y50 to just jog x or y axis to like 50mm.

So to home, I would M18 first, then move head to 0, manually, then G92 X0 Y0, then I would move it using the web control interface (not manually as this will loose the homing) more towards the center. Then I would drop the Z to the bottom manually, then G92 Z0 to set Z to 0. Then bam you can just move as you wish using g-code. Sadly, you can't Z center on 0,0 because of the bolt there. This was something I didn't take into account beforehand sadly.

Sweet, I think that's it. Let me know if you need anything else. Email or text me or whatever.

Also this ended up being longer than I thought it would be RIP. I included a joke underneath the signature since you made it this far.

Best of luck!



<br><br><br><br><br><br><br>


A lil joke:

Why do seagulls fly over the sea?

Because if they flew over a bay, they would be bagels.
