# RCAT 0040

![cat0040_front_1](media/CAT0040_front_good1.jpg)
<br><br><br><br>
![cat0040_first](media/CAT0040_first.png)

Note: previous notes files cane be found [here]()


## what it is
The [CAT](https://en.wikipedia.org/wiki/List_of_spacecraft_in_the_Culture_series) is meant (largely) to serve as an easily-replicated, well known machine that can be put to use on a range of common fab processes: pcb milling (or light milling: i.e. molds), 3d printing (swappable extruder heads would be cool), pen plotting (etc), and knife-cutting...

A few design goals are held in mind for this machine that could make or break the possibility of this machine:
This machine is meant to be made with plastic using mainly a laser cutter and set of 3D printers. The first version of this was made using acrylic. Moving forward, Delrin will be used with perhaps a hint of acrylic here and there.

Total lasercut runtime: ~ 5 hrs
Total 3D print runtime: ~25 hrs

The machine CAD is also parametric, meaning you can tune the width, length, depth, structural strength, height, etc.

## why/how it works
Plastic is not the first place engineers usually go when designing this kind of machine. Metal rules most of this high load machine kingdom because of its great properties that allow flexibility in design choices & maintain stiffness. Using plastic, every avenue to strengthen and rigidize the machine needs to be taken. Instead of using a thick steel sheet for a bed, 2 1/8" plates with an iso-grid web in between is used. Instead of a single plate connecting the x carriage to the y carriage, it's a more boxy structure. That's the main premise - if we turn parts into more boxy ones, we can get the necessary inertia to maintain enough stiffness.

## getting parts ready

### 3d-print

  Carriage & final carriage

  - 3x belt clampers
    4:58 13.43m
  - simple beam joinery
    - 15x T joins
    - 14x simple joint
    5 hours 14m
    - 1x pulley + 1x motor tensioner
    1:14 h 4.66 m
- extra bearings
   30 mins each 0.78 m
  y-axis/rail-to-bed-webs
  - 20x simple joint thick
  - 10x wide_t
    7:14 hr, 20m
  - 32x simple joint
    6:02 hr, 15.4 m
  - 2x pulley + 2x motor tensioner
    2:28 hr 9.32 m
  - 4x endstops
   1:58 7.2 m



### laser cut

Before we go on to cut a bunch of stuff, we need to talk about some basics of setting up the laser cutter. Since everything here is meant to be semi-precise, we need to have our cutter and parameters up to par to ensure the machine will work as intended.

Machine adjustments:
  - Make sure the laser cutter is cutting straight. This is the first point of failure and can cause parts to not fit quite right or not fit at all.

Design adjustments:
  - Measure your material thicknesses. Variations in this can cause the current design to not work. Currently the setup is for thickness of thick sheet to be 6.75 mm and thin sheet to be 3.2mm (as measured on McMaster sourced Delrin used). On each part for the CAT, make sure to adjust the t_MAIN and t_SECOND variables to match your specific material. Note: even 0.1 mm is enough to mess up tightness of system. You will need to test this.

Kerf compensation:
  - The laser does not cut a 0 thickness line. the laser itself has a certain cut thickness and that changes how the pieces will fit together. In each part, you an see variables for "squeeze_factor" or "kerf offset". Squeeze factor should be -2 * kerf offset. For the setup of the laser we are using (Trotec Speedy400), the kerf offset that works is 0.1mm. Note: You **will** need to adjust this to make it tight. Below, I have outlined a procedure to tune these parameters so that everything works nicely.

Setup for cutting Delrin:

Cutting 1/8" Delrin is super easy. 3/16" is also relatively okay. Both of these are done well on a 50+W laser on a single pass

 1/4" can start being a pain. You see quite a bit of melting and re-solidfying that causes parts to re-adhere and makes them hard to take out. After cutting quite a bit of 1/4" delrin, I have found that the best way is as follows:


Inside & Outside cuts: double pass with highest power (tune speed until it cuts nicely through) + if seen not cut through, single pass with highest power and 2x speed than previous cut. This will help get rid of the small bits that get stuck at bottom.

Even with this, you might need to force parts out. Feel free to use the power of shear forces for this (hammer the life out of the sheet). To punch inside parts out, you can get a fancy tool, but I like to just use my keys or literally anything that won't break.


Note all values as follows include prep and taking out parts.

  - carriage
    - Side plates x2
      - 12 mins each
    - side L supports x2
      - each 8 mins
    - Bottom face x1
    - Front facex1
      - Note here, the kerf offset is on the diag web not on the slots like usual
      - 30 mins
    - Diag webs x7
      - Needs to be adjusted for kerf A bit of testing

  - final carriage
    - Front face
    - top face
    - 2 side supports
    -
    total 20 mins cut including prep.
  - Y axis and Bed-rail-attachments
    - 2x v-rail
      - 15 mins each
    - 2x h-rail
      - 15 mins each
    - 16x rail diag webs
       8 mins
    - 6x short rail webs
      - 20 mins including 1 extra
    - 4x long rail webs
      - 13 mins
    - 8x side units
      - 8 minutes



### prepare
   - heat insert all connectors and necessary parts.



## Mechanical

WARNING: Plastic deforms easily so **DO NOT OVERTIGHTEN**. Could also lead to cracking





The machine can be divided into a few core parts:
  - The X-carriage
![x_carr1](media/CAT0040_Xcarr1.png)
  - The Z-carriage
  - The end effector
  - The Y carriages
  ![y_carr1](media/CAT0040_Ycarr1.png)
  - The bed.
  ![bed](media/CAT0040_bed1.png)

For the remainder of this document, I will refer to specific groups of components by the following:

**webs** - usually 1/8" laser cut pieces used to structurally support main sheets
<image of webs>

**connectors** - these are 3d printed pieces used to connect sheets of laser cut plastic together.

In order to join the plastic laser cut sheets, 3d printed joinery is made. There is a few types of joinery. The bed uses HexTrompo joints. Named that because they look like the toy commonly used in Mexico: [Trompo](https://en.wikipedia.org/wiki/Trompo). These little dudes help tension the two bed sheets together with the webbing pieces sandwiched in the middle. There is a male one which lies on the top and a female one with a heat insert on the bottom sheet. An M4x55 bolt runs between them and keeps it all nice and taught. This way, all the loads that go on to the bed first come through a 3d printed part softening the load on the acrylic hopefully preventing some of the cracking if you use acrylic. A whole bed's worth should take about 10 printer hours to make.

![beam_joinery](../media/beam_joinery.jpg)

Here is a unit cell of what the inside of the bed looks like:

![ht_unitcell](../media/ht_unitcell.jpg)

For the side walls, a different kind of joint is used. These don't have a name yet so we'll call em simple beam joints here. They are designed to click in and be easy to put on and tighten.

Here is a unit cell of what these beam joints look like:

![bj_unit](../media/bj_unit.jpg)


Let's talk about how to assemble:

###  Bed
![bed_pic](media/Cat0040_bed_frontview.jpg)
![bed_zoompic](media/Cat0040_bed-zoomed.jpg)

The bed is assembled with The HexTrompo connectors. These work as tensioners to sandwich the webs between the two large bed sheets.

1. Place all the female HexTrompos(ones with heat insert) required for the bottom sheet.
2. Place all your webs into the appropriate holes in the bottom sheet. Add the rail webs while you are at it to save time later.
3. Sandwich top sheet on top. You will have to align the webs. Do this from one corner to the other, moving the webs underneath through the holes meant for the HexTrompos. After the sheets are nicely fitted, add the male HexTrompos and tighten using M4x55 flathead bolts.


### X - carriage
![x_carr](media/CAT0040_finalcarr-diagview.jpg)
Here you have a cool thing, where there is an extra set of bearings that help clamp onto the railing. These are the ones on the 3d printed black parts. They ride on the face of the cut and help deal with rotational moments.

Place the appropriate webs into place, and just follow the images/CAD. This one is pretty self explanatory.

### Y - carriages
![side_view](media/CAT0040_ycarr-sideview.jpg)
If you have not yet installed the rail webs, do that.

Once webs and rails are together, the whole contraption should fit nicely onto the bed. Make sure the connectors are in the right place before you go to attach it on, otherwise, you won't be able to attach it. Everything is tightly press fit for maximum rigidity, so you might have to hammer things in depending on the adjustment that you put on.



Attach everything together
![side_connection](media/CAT0040_side_attachment1.png)


## Carriage bearing preload
![bearing_setup](media/CAT0040_bearing_setup.png)
The bearing are setup so that one is static and the other is on a beam H-bar type tensioning system in order to prevent it from jamming with all the other bearings since it is over-constrained. There is a preload of 0.2mm on this bearing

Maths:

157.68 N of preload for the 0.22mm deflection on a link with width 3mm, height 5mm and depth of 6.50mm
Simple myosotis beam equation. Note this assumes the bearing are perfect, in reality, a portion of deflection will not be translated perfectly, leading to less preload force. Either way it is acceptable

## Belt tightening/clamping and pulley system

![belt_tighten](media/CAT0040_ycarr-clamping.jpg)

The belts are setup to be tightened on the motor side. There is 3d printed tensioners that push on the motor and move the motor more towards the side. The motor can rotate about one of its bolts (bottom), so as to be able to move maximum of ~1mm horizontally - enough to get enough tension after manual tensioning at the belt.

On the carriage side, there is 3d printed belt clampers which hold on to the belt and connect it on both sides. Each side is bolted down separately.

In the picture above, you also get to see the pulley side 3d printed pulley.

Here it is again:

![fulcrum_tensionser](media/CAT0040_fulcrum-desc.png)

With the pulley we want to provide that pulley support as well as belt tracking (keeping the belt aligned with center of bearings and away from rubbing on sides which leads to wear). The bottom of the middle portion is bolted down, and a bolt attached through an insert on the plate, can push up on the side - making the center portion a fulcrum allowing rotation of belt. As we can also see in picture, the belt is meant to be above the fulcrum point in order for tension to make it push up against the bolt, making it stay in position no matter how much tension is put.



#### Energy chains.

Make two of these for the Y and X axis': [link](https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/tapeChains). These will let you wire this puppy (cat?) up with no problem.

#### Electronics

Here is a picture of the Duet3D layout to help tell what is what: https://d17kynu4zpq5hy.cloudfront.net/igi/duet3d/vqBUAZPsxMC5tRgt.full

There is also a picture of the board connected with these motors onto the CAT 0020 attached.

Mount the Duet 3D with M4 bolts with the motor connector side toward the front onto the Duet 3D printed mount. Plz don't super tighten those. They're supposed to be loose because the mount I designed is super bad and you don't want to bend the board. Connect power (+ is labeled) and the motors. the X axis goes on  Drive 0, the Y axis motors go on Drive 1 and 2 (The right side motor on Drive 2), and the Z on Drive 3.

That should be it for that? Make sure Black/brown matches the way it is in the picture of the Duet3D layout.


#### Software
I'm gonna leave this part to Duet3D's guide. It's super detailed and easy to follow. You can find it here:
https://duet3d.dozuki.com/Wiki/Step_by_step_guide

Eventually, you'll get to the configurator to setup the Duet3D with this machine which is online here: https://configurator.reprapfirmware.org/

It's super intuitive and here are the values I changed for the CAT 0020:

General Tab:
Cartesian:
X range(0-325) Y range(0-330) Z range (0-50) <this Z range is too big with the bed, you can change it to 0-34 if you want hard stop including the bed which you probably want.

Motors Tab:
Axes:
All drives shoudl be x16 microstepping, 80 steps/mm, 15 mm/s speed change, max speed of 500 mm/s,  1000mm/s^2, and 800 mA of motor current. These are not tuned AT ALL. I turned them up to get better performance, but I didn't test too much, so feel free to reconfigure later if you want and send me back what you put so I can reconfigure here too.

The Z axis should say that the  Motor driver is on 3. (this doesn't matter though I think because it gets overriden later anyway...)

One thing that matters is the IP address on the Network page. What I did is I took the IP address that it connected to dynamically in YAT when I had it first connect to WiFi and just copied that into the  IP Address slot after you uncheck acquire dynamic IP address. I'm not too knowledgeable on networking though so I'm not sure how this will work out over there. This might be a place where you run into trouble. I did read that it was possible to still send gcode with just the USB connection through like YAT though, so since the hardware is already configured here, it should still work through that worst case scenario?

In the Finish Tab, it's important that you add the following two lines to the Custom settings (just copy paste it). It just sets up the duet to use 2 motors for the y axis, and reverses one so they don't fight each other. This also means that the silkscreen markings for the Axes on the duet are wrong with this setup though so beware there.

M584 X0 Y1:2 Z3 E4:5 ; set 2 and 3 to y axis, whilst putting z in motor 3 slot  
M569 P2 S0 ; reverse one of the y motors (Motor 2)



The rest I leave at default or if they're obvious like the endstops setting to None or setting Heat Bed to none. If you can figure out the stall detection though, that would be awesome. It didn't work when I tried it out a few times with different parameters, but with more time, it might be usable.
After you finish configurator you just upload the zip using the web tool.



#### G-Code
This website details all the codes it accepts and it's super useful: https://duet3d.dozuki.com/Wiki/Gcode

The main ones I used to test and home:

M18 - disables motors.
G92 X0 Y0 Z0 set this x,y,z position to be 0 (if you only do G92 X0, it will only set X to 0)
G90 - absolute positioning
G91 - Relative positioning
G0/G1 Xnnn Ynnn Znnn Fnnnn  Jog to x,y,z there with speed Fnnnn where nnnn here is the mm/min. I found F30000 to be around the maximum. After that it can skip and stuff. Here too, you can just say X50 or Y50 to just jog x or y axis to like 50mm.

So to home, I would M18 first, then move head to 0, manually, then G92 X0 Y0, then I would move it using the web control interface (not manually as this will loose the homing) more towards the center. Then I would drop the Z to the bottom manually, then G92 Z0 to set Z to 0. Then bam you can just move as you wish using g-code. Sadly, you can't Z center on 0,0 because of the bolt there. This was something I didn't take into account beforehand sadly.

Sweet, I think that's it. Let me know if you need anything else. Email or text me or whatever.

Also this ended up being longer than I thought it would be RIP. I included a joke underneath the signature since you made it this far.

Best of luck!



<br><br><br><br><br><br><br>


A lil joke:

Why do seagulls fly over the sea?

Because if they flew over a bay, they would be bagels.


### Notes

The RCAT 0030 is very solid when it comes to the base. The base is quite rigid in all degrees of freedom, however, it is easy to see its weak point in retrospect: the attachment from the x carriage to the bed. There is a diving board effect that happens at the edges and it could lead to major deflections. Thus the search for a more rigid structure is here.


# Side face attachments

The vertical side face needs to attach to the horizontal side face at some point. There is a desire to maintain the base as close to the 0030 as possible.   So for the 0040, the edge rail webs will be swapped out to fit a more boxy way to attach rails for the bearings.

There is a general design for how this will work:

<picture of the general sketch

# torsion on x axis
![torsion_img](media/test_x_current.png)

As we can see from the picture above, the initial design suffers too much under torsion. I tried three different changes and submitted them through a FEA test, and the last one proved much better, improving performance by about 85% by adding a third bar at 45 degree angle to horizontal. As we can see from the stress test, the stress is much more evenly distributed this way as opposed to being concentrated on where the carriage is. Now I must redesign x axis to incorporate this somehow.

It's nice, I think I can reuse most components and only swap out the middle angled brackets and run the bar between them. Let's test it out.

![CAT0040_wall](media/CAT0040_w_all_far.jpg)
It's really bad, it jiggles wayy too much. I'm talking about 3mm with barely a tap, and ~20mm with some force.

This is going to require quite a bit of redo on x-axis. I will move to Jake's beamish design with a closed beam around it.

If I add a vertical bar at the tip, I also get massive gains. 8x improvement from 8mm to 1mm



# Major lesson: CLOSE BEAMS

### DDMCs

Time to bring in Jake's controllers into the equation. I am taking a break from fixing torsion on X-Axis to figure out these controllers and try to make them work.

I pulled Cuttlefish & Nautilus, let's see what happens.
