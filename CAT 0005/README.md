# The Clear Air Turbulence

![catone](catone.jpg)

The [CAT](https://en.wikipedia.org/wiki/List_of_spacecraft_in_the_Culture_series) is a bed-dropping machine implementing a swappable tool system.

This is meant (largely) to serve as an easily-replicated, well known machine that can be put to use on a range of common fab processes: pcb milling (or light milling: i.e. molds), 3d printing (swappable extruder heads would be cool), pen plotting (etc), and knife-cutting.

To do this, I'm going to develop in parts. The top (CA), a low-profile plotter using (1) of [these pulley axis](https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/gantries/n17_linearPulley) on the spanning x-direction, with a modified set of (2) of them on the y-axis (split). The bed (T) a gear-reduced [similar but more-gusto-having axis](https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/gantries/n17_heavyPinion).

I'd like to take this chance to develop some chassis practice. I have been working through [making decent structures from plastics](https://gitlab.cba.mit.edu/jakeread/rctgantries/tree/master/beams) and I'd love to develop a tool (probably rhino/gh) to 'tab-out' arbitrary planes. I think if I simplify these worlds so that (1) only one 'type' of intersection exists and one type of printed part. The part should be small, leaving most of the 'work' to be done with shear through the insert. As a revolve, I can make this world thickness-agnostic as well. Feels like a worthwhile, easy-ish rhino/gh project, that I hope would serve me in the future.

## Dev Log

Ok well here's the idea of a machine, lots to do.

At this point, notes are for the haystack instance.

To scale, I'm going to fit the width to a 24" sheet, so the hoop gets 18" of travel (roughly) (update: 15.8").

OK ... motors to the back: out of harm's way when loading clay etc, do by flipping motor - pulley-tensioning axis -> also nice for tensioning and mod tracking pulleys.

Watch that bonus ~ 60mm on the back y-bar ...

I had some trouble annealing something that feels like a solid structural loop. It still feels a bit kludged, but I think the best way to rev is to make one. So.

Next task is to try this rhino/gh tab-geometry-proliferating device.

Still needs:
- beam geometry
- headgear (front set: pin the U-mode)
