# R-CAT

## Goals
Create a good overall gantry frame to allow for several types of machining such as 3D printing, high-speed milling, or circuit etching.

## Notes

##### 6/24/2019

###### Initial Assumptions/Goals/Concerns
   There's a few assumptions I want to make mainly:
  - Main large forces will be sideways on the end effector as in something like a milling machine.
  - I want to reduce as much play in the machine as possible for accuracy
  - Z axis movement will be installed separately and will be independent of X & Y. If installed, it will live as an end effector.
  - With the U-frame design with mid beam, using only bearings on side per beam means that effectively, you are only using one beam at a time. Which is no bueno  when you are on the edges because it turns into a classic beam with force at the end problem which can bend significantly under load. If I can clamp the side beams to the ground this can work.< I like this idea quite a bit. Let's call it the **clamped XY** idea


Measurement specific-
  - Stolen from CAT Mid beam width 100 mm



###### Horizontal vs Vertical  XY beams
Researching routers, horizontal and vertical approaches are both taken.
  - The larger distance between bearings allows vertical to be marginally better at handling moments in x the axis.
  - Horizontal involves less big parts which makes it simpler and easier to mount. It also requires less new designs for beam parts.

The decision then comes between easier, simpler, and mostly already designed vs slightly better performance.

I think the slight performance increase is not worth the effort. Additionally, the performance increase is only theoretical.

While initially, I wanted to move to a vertical design, the **horizontal design** is the one I vouch for now.

###### CAT 0.1 Review (Clamped Bed)
  - The main beam members are not  loaded to best for sideways forces on end effector for something like a mill. If we put the X & Y members so the bearing face is facing inwards, we can use the side faces to support bending as that is the biggest mechanical factor in a machine of this type.
  - The diagonal bearings I still don't like due to the minimal contact area between the bearing and the face, however, I will roll with it because it is easy to change later if it does become a problem, and it is Jake's current method.

Overall thoughts: A design like this doesn't allow for a lot of force to be exerted, however, it allows for a huge effective working volume as the whole X & Y gantry moves around as opposed to just the Z axis.

######  Clamped XY CAT
  - This significantly supports the x & y axes, reducing the bending of these beams leading to more accuracy and increased sideways load capabilities on the end effector, allowing for something like a router.
  - In just about every router I can find, this is the solution that is taken.
  - Electronics could be mounted on the either side with the bed being on 3D printed supports underneath connecting it to the ground.
  - A con of this design is that it doesn't clear much z axis without the mid beam getting in the way of potentially 3d objects.
    - Could be fine for high speed milling, etching circuits, vinyl-cutting, laser cutting.
    - Could be not fine for something like 3d printing, or milling >2" vertical objects.

I just realized this looks just about exactly like the little rascal. It's like a laser cuttable litte rascal that's thicc-er to allow for using acrylic.

<<Picture of Rough CLAMPED XY

Overall thoughts: This design is a pretty good overall option. It is simple and its similarity to little rascal means some of the experience there can translate. Additionally, the fact that most parts remain static, means there is less movement and thus room for error. The lack of effective Z range, however, can be a game changer.

<<Picture of ROUGH CLAMPED XY BOXED

###### Extruded aluminum?
Use 2x1 extruded aluminum to save on most of the complicated-ness of it all. Could fabricate most parts on a standard manual mill. The question would be. Is a standard manual mill in the budget?

Aluminum has much better properties, and tubing would be just as expensive as acrylic, and less expensive than running a waterjet.

2x1 1/8" 6ft length (enough for 1 machine) - $53 on mcmaster

##### 6/25/2019


I am going to go forward with v1 of the Clamped XY idea. I have a feeling the boxed version using Jake's beams will be the one going forward, so I will CAD a mock of that first up. It should be fairly simple, as it is boxy and does not require much.

###### Linear rods & bearings

This idea has a lot of potential I think. It is similar to the gantry Emily and I made this previous term (Spring 2019) and it would be the simplest & cheapest. You do lose performance though because you cant support it throughout the rod, only in the ends. For some applications, I think this has potential.

Let's analyze situations for different kinds of linear motion

###### 3D printing
Ex: Prusa mk3
For 3D printing we see two main linear motion methods: high-quality rods/bearings vs v-wheels. To move, mostly belts and ball screws are used.

V-wheels wear faster, can get dirty easier, and are less stiff. Linear rods or bearings on a rail are both the best practice for 3D printing. That being said, either way 3D printing does not put much load on your gantry, so either can work.

Along with that it is interesting to note that the high z space is due to the X&Y axes being moved along with the end effector on most 3d printers. This prevents it front hitting parts on the bed with tall parts.

###### PCB Milling Machines
PCB Milling machines see mostly linear rod motion with ball screws. They exhibit low forces like 3d printers, however unlike the 3d printers, they don't need a high z space, so the z axis is the only one that moves in the z direction.

Most of these also have enclosures.

###### Desktop CNC routers
These look an awful lot like the PCB milling machines except more beefy and without an enclosure. Most of these use around 1/2" rods.

###### Desktop laser cutters
Ex. Dremel Digilab Desktop Lasercutter
Linear rods & belts.

##### Conclusions from looking at these Machines

For the most part, linear rods are being used for desktop-type machines. I believe this is due to their simplicity & cost at a reasonable performance. Since these desktop machines do not have super long beams, they can get away with the fact that they aren't being supported throughout the machine. I am interested to see the load capabilities of using the linear rail system vs the linear rod system while retaining a reasonable accuracy. 3D Printers are the only use case I saw where that tall Z space was needed. I don't think the cost of getting that is worth it. You would have to make the whole X&Y axis move, or make the bed and the whole Y axis move. This would make the machine bigger, while loosing the capability for higher loads which is more desirable. Perhaps you can make a 3d printer by creating a custom shaped end effector that reaches up and then down to avoid collisions. But that is super janky.

From this, I shift my position from using box tubing to using linear rods. Tubing would be better for bigger machines, but for this form factor, the linear rods should be just as capable while being reasonably cheaper & simpler. If the rail or tubing method can demonstrate supreme loads reasonable enough to carve on to lets say... aluminum though... that'd'be dope and may be worth the extra complexity.

McMaster
Linear rods Cost-
1/2" dia - 8' = $61.4
High Load Linear bearings
1/2" ID - Holy crap I thought these would be like $5 each, but theyre like 40$ each. There's no way... I take it back. Ball bearings all the way.

Looking more and more at mcmaster... maybe aluminum bars would be the way to go?

###### bed

For any design, the bed would be either water-jetted 0.25" aluminum or 0.25" acrylic depending on machine use. The main idea is to raise the bed in order to mount the electronics in the bottom. This will make it package nicely and make it look all prettied up.

##### 6/26/19
Spent CADDING up the RCAT

<< picture of RCAT

##### 6/27/19
Design review today, I am cleaning up the edges around.

###### the deets

McMaster 1942K181 as feet for the bed. The bed now has the appropriate holes for the side beams, and now I want to make holes for all the necessary electronics as well as general mounting holes.

###### Bed part 2

I think it would be cool if bed was interchangeable. This is because there is wildly different cases that might require different beds. As such, I want to leave some area of the bottom hole-less in order for it to be able to be used  for simple stuff...  maybe like PCB milling?. Then mounting holes around it to attach a new bed or like a vice for something like routing wood.

###### 6/28/19

###### Jake Response

Alright. I think it's on its way. I agree with you on the Z-height, we'll want to be able to crank that up without getting rid of the structural loop.

Take a look at the 'CAT ASM' file in 'RCT Gantries / Clear Air Turbulence' in Fusion. There's a few things there that I'd like to see in the CAT. The first is this 'C' shaped structural spine. That's a totally necessary part of the design - I was hoping this machine would be more integrated than the Little Rascal is. The second is the size of the X Beam - in the CAT ASM it's quite deep - this is roughly as deep as it is in the Little Rascal, which makes it easy to route the belt through the back, and to attach all of the end effectors, or a separate z-axis.

Don't worry yet about mounting power supplies, or routing cables etc. Lets pin down the structural loop and gantries first - those need to be solid. Just see if you can get X and Y worked out in a satisfying way.

Also keep in mind that it will be nice to minimize the number of 3D Printed 'beam-joint' elements.

Final thing: there's a new model of the 'RCT N17 Linear Pulley' that's in 'RCT Gantries / Gantries / RCT N17 Linear Pulley V2' - that has some updates Samuel made that we should include.


On using tubes, or extrusion, let's stick to the beam designs for now. There is some desire to stay away from purchasing key parts ... and cutting tubing like that to size well is not always easy. Extrusions are not always very straight either, but a laser-cut edge with laser cut supports will at least approximate the straightness of the laser's geometry.

Please make sure you get the point about the 'C' structure, yeah?


###### notes:

might need to buy more wall connectors


##### 7/1/19

###### Materials (in)
0.25"  
C-top:17.323"x26.378"  &2.384
bed:17.323"x26.378"
carriage: 25"x4"
[: 27"x5.5"/3"
carriage carriage: 4.5"x4.5"

Total:
 2 sheets of 24"x36"x0.25"
or 3 sheetse of 24"x24"x0.25" with redesign

0.125"

side sides: 15.7"x2.7" x4  
mid side & carraige sides: 21.5" x 2.7" x4  
all the vertical webs: 2.7" 1.5" x 20
diagonal webs: 4" x 1.5" x 20

Total 2 sheets of 24"x36"x0.125"

What else do i need?

Bolts- Got all the M3s we need  
Carriage stuff  
  - Bearings- got a tons
  - 5x6xM4 shoulder bolt x 8
  - 5x20xM4 Shoulder bolt x 3
  - bearing shims pk 25 x1
Foots  
~~M5 Spacers~~  
Bearing holders stuff.

##### 7/3/19

###### Laser Cutting baby

What do I need?

Frame:
  - 0.125"
    - 4x Side Beam sides
    - 1x Mid Beam back side
    - 1x Mid Beam Front Side
    - 4x C-Vertical Webs
    - 3x C-Diagonal webs
    - 8x S-Vertical Webs
    - 6x S-Diagonal webs
  - 0.25"
    - 1x C frame
    - 1x Full Bed

Carriage:
  - 0.125"
    - 5x Vertical Webs
    - 4x Diagonal Webs
    - 1x Side Side
  - 0.25"
    - 1x '[' frame
    - 1x Front face

End carriage:
  - 0.25"
    - 1x Bearing System

##### 7/7/2019

###### SIKE
![sike](media/sike.gif)

<< insert picture of 1/4 finished CAT

There's a few problems with the CAT. I don't think this one is meant to purr.

- Main problem is with the belt routing. The belt being routed through the webs causes a lot of problems. I put a cutout on the top of the webs, however, its not big enough so there will be tons of friction there which can cause the belt problems after some hours of being worked.
- The structural loop is not so structural.

It was a good attempt, but this next one is gonna be even better.
VVVV

## RCAT 2.0 (which is actually 0.2)


![almost](media/RCAT-0_2-close.jpg)

The main structure of this is much different than the others'.
After discovering that the 0.1 just has wayyy too much give in the rails and just overall everywhere, a re-design needed to be done.


New thangs:

  - Flipped side Bearings - running on inside track again...
  - New Beam Structure
  - New overall Structure
  - No more C... I never liked it.
  - Supreme


##### laser Cutting

Laser Cutting
  -  large bedsx1 DONE
  - small bedsx1 DONE
  - horizontal web x13 DONE
  - diagonal web 5x24 DONE
  - rail web x6 DONE
  - large rail web x4 DONE
  - side wall x2 DONE
  - rails x2 DONE

##### Hardware

Gotta do a hardware check to see what to use and what we will need.

Bed
  - 20x flathead M3x20
  - 10x sockethead M3x20
  - 10x M3 washers
  - 18x  M4x55
  - 4x  M4x65or M4x70
  - 22x M4 washer if sockethead
  - 18x M4 heat inserts
  - 30x M3 heat inserts
Carriage
  - 10x flathead M3x20
  - 30x sockethead M3x20
  - 30x M3 Washers
  - 8x flathead M3x~15
  - 16x 0.5" self tapping screws
Z axis
  - 16x 0.5" self tapping screws
  - More M3 heat inserts and M3x20ish but there's tons of those so no worries

##### Improvements for later

- Explore idea of putting motor mount on laser cut to save time.
- 1/8" in place of 1/4" in some places to be able to use delrin since 1/4" delrin is a pain. doable but a pain.
- More vertical webbing for the rails. Basically just extend bed diagonal webbing into the rails.
- Redesign Bearing design to full contact. Possible by using V shaped rails.
-





##### SETUP OF THE DUET 3D Wifi

Just follow online instructions bro.

Configurator:
x maximum: 325, y max: 330 z max:50

Need steps/mm
All axes use 20T pulleys directly driving a GT2 belt.
Nema17 motors that we use are 1.8 deg/rev at 200steps/rev
then 20 * 2mm/rev : 40mm/rev. this means 5 steps/mm. Although apparently there is miro stepping? at 16/ step so 80 steps/mm totla?



## Putting it together from travel kit

Packed are three main components along with a bunch of hardware and the duet3D inside of its box. The main components are the bed, the carriage (X axis), and the Z axis. I'll be referring to them like this from now on. It should be fairly obvious which one is which.

Before you start:
  - I attached a ton of pictures so look at those as need be.
  - I warn you that all the components are tethered together so be careful as you take them out.
  - I put a lot of the bolts you need on the heat inserts/nuts of where they are supposed to go, so you if you see a bolt sticking out, it means something goes there.
  - The belt and the wiring are long and can tangle themselves with other parts and can get trapped when you put in the end stops. Make sure to stretch these out before you fully install the carriage for example, so as to avoid having to re-do it.

Most of it is fairly obvious on how to put it back together mechanically, so I'll keep this part concise.

I would first reinstall the z axis onto the carriage. It goes on the kinematic mount with two M4 bolts (which I left on in the carriage heat inserts, you'll see them sticking out). Then you should zip tie the end of the wiring harness that goes to the z axis. It attaches to a mount that sticks out of the top of the carriage.

Next, put in the guides for the wiring harness on the side of the bed. These are the pink 3d printed parts that look like right triangles.

Now you should re-install the rails. You should first put in the beam joints (the green 3d printed parts in the hardware bag) the 3 sided one goes on the outside edge and the 1 sided one on the inside. These just slide in and click in. Make sure your orientation is right (the vertical portion should be close to the top so as to guide the bolt, look at picture if need be). The rail goes right on top of that. I left the endstops and pulleys installed so you shouldn't need to retune the tracking. Make sure you use the right rail on the appropriate side. You can tell easily by the orientation of the hardstop. The hardstop should be on the inside to actually stop the carriage. The rails go on with M3x20 flathead bolts into the beam joints. They're labeled in the hardware bag.

Slide the carriage into the bed rails from the front, and then zip tie the wiring harness to the pink wiring harness guides.

Lastly, install the violet motor mounts at the front of the rails centered on the holes. Stretch out the belts and run it through the pulley on the rail and grab the pulley pinions in the hardware bag (20T B5)and install onto the motor mount along with the belt. The vertical position of the pulley matters somewhat, so look at a picture and try to mimic it. Tighten up the motor afterward until you think belts are nice and tight.

Electronics
______________
Here is a picture of the Duet3D layout to help tell what is what: https://d17kynu4zpq5hy.cloudfront.net/igi/duet3d/vqBUAZPsxMC5tRgt.full

There is also a picture of the board connected with these motors onto the CAT 0020 attached.

Mount the Duet 3D with the bolts labeled in the hardware bag with the motor connector side toward the front. Plz don't super tighten those. They're supposed to be loose because the mount I designed is super bad and you don't want to bend the board. Connect power (+ is labeled) and the motors. the X axis goes on  Drive 0, the Y axis motors go on Drive 1 and 2 (The right side motor on Drive 2), and the Z on Drive 3.

That should be it for that? Make sure Black/brown matches the way it is in the picture of the Duet3D layout.


Software
_______________

You'll have to restart all the firmware stuff here because you'll be using a different Wifi.

I'm gonna leave this part to Duet3D's guide. It's super detailed and easy to follow. You can find it here:
https://duet3d.dozuki.com/Wiki/Step_by_step_guide
Once you're in the Web Control for the first part using YAT, the password I put in is welikepancakes all lowercase.

Eventually, you'll get to the configurator to setup the Duet3D with this machine which is online here: https://configurator.reprapfirmware.org/

It's super intuitive and here are the values I changed for the CAT 0020:

General Tab:
Cartesian:
X range(0-325) Y range(0-330) Z range (0-50) <this Z range is too big with the bed, you can change it to 0-34 if you want hard stop including the bed which you probably want.

Motors Tab:
Axes:
All drives shoudl be x16 microstepping, 80 steps/mm, 15 mm/s speed change, max speed of 500 mm/s,  1000mm/s^2, and 800 mA of motor current. These are not tuned AT ALL. I turned them up to get better performance, but I didn't test too much, so feel free to reconfigure later if you want and send me back what you put so I can reconfigure here too.

The Z axis should say that the  Motor driver is on 3. (this doesn't matter though I think because it gets overriden later anyway...)

One thing that matters is the IP address on the Network page. What I did is I took the IP address that it connected to dynamically in YAT when I had it first connect to WiFi and just copied that into the  IP Address slot after you uncheck acquire dynamic IP address. I'm not too knowledgeable on networking though so I'm not sure how this will work out over there. This might be a place where you run into trouble. I did read that it was possible to still send gcode with just the USB connection through like YAT though, so since the hardware is already configured here, it should still work through that worst case scenario?

In the Finish Tab, it's important that you add the following two lines to the Custom settings (just copy paste it). It just sets up the duet to use 2 motors for the y axis, and reverses one so they don't fight each other. This also means that the silkscreen markings for the Axes on the duet are wrong with this setup though so beware there.

M584 X0 Y1:2 Z3 E4:5 ; set 2 and 3 to y axis, whilst putting z in motor 3 slot

M569 P2 S0 ; reverse one of the y motors (Motor 2)



The rest I leave at default or if they're obvious like the endstops setting to None or setting Heat Bed to none. If you can figure out the stall detection though, that would be awesome. It didn't work when I tried it out a few times with different parameters, but with more time, it might be usable.
After you finish configurator you just upload the zip using the web tool.



G-Code
______________
This website details all the codes it accepts and it's super useful: https://duet3d.dozuki.com/Wiki/Gcode

The main ones I used to test and home:

M18 - disables motors.
G92 X0 Y0 Z0 set this x,y,z position to be 0 (if you only do G92 X0, it will only set X to 0)
G90 - absolute positioning
G91 - Relative positioning
G0/G1 Xnnn Ynnn Znnn Fnnnn  Jog to x,y,z there with speed Fnnnn where nnnn here is the mm/min. I found F30000 to be around the maximum. After that it can skip and stuff. Here too, you can just say X50 or Y50 to just jog x or y axis to like 50mm.

So to home, I would M18 first, then move head to 0, manually, then G92 X0 Y0, then I would move it using the web control interface (not manually as this will loose the homing) more towards the center. Then I would drop the Z to the bottom manually, then G92 Z0 to set Z to 0. Then bam you can just move as you wish using g-code. Sadly, you can't Z center on 0,0 because of the bolt there. This was something I didn't take into account beforehand sadly.

Sweet, I think that's it. Let me know if you need anything else. Email or text me or whatever.

Also this ended up being longer than I thought it would be RIP. I included a joke underneath the signature since you made it this far.

Best of luck!

Regards,
Ruben Castro


Why do seagulls fly over the sea?

Because if they flew over a bay, they would be bagels.


# What's next?
I gotta make the CAT 0030.

I want to prototype a bunch of stuff though.

What worked well:
The bed worked surprisingly well. It was quite rigid in the main required degrees of freedom, and I want to maintain that the same.

What didn't:
  - Mounting stuff.
  - acrylic
  - vertical belt mounts.
  - time consumption of carriage making


What to change:
- Add webbing to the rails on the other side.
- Orthogonal bearing contacts
  - this requires prototyping of V rails, simple rail with orthogonal bearings.
- Explore using more 1/8" as opposed to 1/4"
- Move to Delrin or other materials.
- re-design beam model with new easier joinery.
- Explore making laser cut motor mounts.
- 6mm hole for mounting switch
- More height clearance


What to prototype:
V-rail.
orthogonal Bearings
1/4" delrin laser cutting tuning
vertical rails

#CAT 0030

Look at new document, this was getting crowded.
